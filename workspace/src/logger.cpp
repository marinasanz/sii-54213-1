#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	char buffer[128];  //almacenaje de lo que recibe
	if (mkfifo("Tuberia",0777)!=0)  //si la tubería se crea mal, se imprime un mensaje de error
	printf ("Problema en la creación de la tubería\n");

	int tuberia=open("Tuberia", O_RDONLY);   //abrir tubería en modo lectura
	while(read(tuberia, buffer, 128))  //bucle infinito de recepción de datos
	{
		printf("%s", buffer); //lee lo que hay en la tubería y lo imprime por salida estandar
	}

	close(tuberia);  //cerrar tubería
	unlink("Tuberia"); //"Desenlazar"
	return 0;
}
